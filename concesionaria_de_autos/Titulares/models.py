from django.db import models

# Create your models here.
class Titular(models.Model):
	apellido = models.CharField(max_length=100)
	nombre = models.CharField(max_length=100)
	telefono = models.IntegerField()
	direccion = models.CharField(max_length=100)
	mail = models.CharField(max_length=150)
	def __str__(self):
		return self.apellido + ' ' +self.nombre

