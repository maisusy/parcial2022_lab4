from django.db import models
from Marcas.models import Marca
from Modelos.models import Modelo
from Titulares.models import Titular

# Create your models here.
class Auto(models.Model):
	marca = models.ForeignKey(Marca,on_delete=models.CASCADE)
	modelo = models.ForeignKey(Modelo,on_delete=models.CASCADE)
	estado = ['en venta','vendido']
	modalidad_ingreso = ['compra','consigancion']
	km = models.CharField(max_length=4000)
	color = models.CharField(max_length=100)
	cant_puertas = models.IntegerField()
	precio_venta = models.FloatField()
	precio_compra = models.FloatField()
	cant_titulates_ante = models.IntegerField()
	titular = models.ForeignKey(Titular,on_delete=models.CASCADE)
	patente = models.CharField(max_length=10,default='')
	def __str__(self):
		return self.patente

	
